from prefs import fields
from django.conf import settings
import os

config = [
    # в основном списке находятся словари. Каждый словарь это отдельная вкладка
    {
        'title': 'Tasks',
        'content':
            [
                # список content содержит словари-секции. Каждый словарь это отдельная секция на вкладке
                {
                    'title': 'Section 1',
                    'fields': [
                        # список fields содержит инициализированные классы полей
                        fields.PrefStringField('VALUE_STR', 'Value String', default_value='', help='Help text'),
                        fields.PrefSummernoteField('VALUE_SMN', 'Value Summernote', default_value='', help='Help text'),
                    ]
                },
                {
                    'title': 'Section 2',
                    'fields': [
                      fields.PrefBoolField('VALUE_BOOL', 'Value BOOL', default_value=True, help='Help text'),
                      fields.PrefSummernoteField('VALUE_SMN', 'Value Summernote', default_value='', help='Help text'),
                    ]
                }
        ]
    },
    {
        'title': 'New Tab',
        'content':
            [
                {
                    'title': 'New Section',
                    'fields': [
                        fields.PrefStringField('PARAMETER_NAME', 'Parameter Title', default_value='',
                                               help='Help Text')
                    ],
                },

            ]
    },


]