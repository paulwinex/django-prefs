from suit.apps import DjangoSuitConfig
from django.conf import settings


class SuitConfig(DjangoSuitConfig):
    layout = 'vertical'

    @classmethod
    def custom_menu_handler(cls, menu_items, request, context):
        # par = ParentItem('Tools', icon='fa fa-chevron-right', url='#')
        # par.children.append(ChildItem('Preferences', url=reverse('prefs')))
        # menu_items.append(par)
        menu_items.extend(cls.collect_items())
        return menu_items
    menu_handler = custom_menu_handler

    @staticmethod
    def collect_items():
        items = []
        for app in settings.INSTALLED_APPS:
            try:
                suit_menu_mod = __import__(f'{app}.suit_menu', fromlist=['suit_menu'])
                menu = suit_menu_mod.get_suit_menu()
            except ModuleNotFoundError:
                continue
            except ImportError:
                continue
            items.extend(menu)
        return items