from django import forms
from django_summernote.fields import SummernoteTextFormField
from django.template import loader
from django.utils.safestring import mark_safe
from django.conf import settings
import logging

logger = logging.getLogger(__name__)
custom_widget_namespace = getattr(settings, 'PREFS_WIDGET_TEMPLATE_NAMESPACE', None)
generic_widget_class = getattr(settings, 'PREFS_WIDGET_CLASS', None)
widget_type_attrs = getattr(settings, 'PREF_WIDGET_ATTRS', None)


class PrefField(object):
    type = ''
    field = None
    widget = None

    template_name = ''
    default_attrs = {}

    def __init__(self, name, label, default_value, help=None, attrs=None,
                 hidden_when=None, disabled_when=None, namespace=None, widget_class=None,
                 widget_attrs=None, **kwargs):
        self.kwargs = kwargs
        self.widget_attrs = widget_attrs or {}
        self.widget_class = widget_class
        self.name = name
        self.help = help
        self.template_namespace = namespace or custom_widget_namespace or ''
        self.error = ''
        self.label = label
        self.hidden = False
        self.disabled = False
        self.default_value = default_value
        self.id = '{}-id'.format(name)
        self.attrs = attrs or {}
        self.hidden_when = hidden_when
        self.disabled_when = disabled_when
        self._value = ''

    def __str__(self):
        return str(self.value)

    def __repr__(self):
        return '<PrefField "{}" ({})>'.format(self.name, self.value)

    @classmethod
    def get(cls, field_type, name, label, default_value, attrs=None, **kwargs):
        for c in cls.__subclasses__():
            if c.type == field_type:
                return cls(name, label, default_value, attrs, **kwargs)
        raise ValueError('Unknown type')

    @property
    def value(self):
        return self._value

    def set_value(self, value):
        self._value = value
        return self

    def set_template_namespace(self, value):
        self.template_namespace = value

    def get_attrs(self, extra_attrs=None):
        attrs = {}
        if 'id' not in attrs:
            attrs['id'] = self.kwargs.get('id', self.id)
        attrs.update(self.default_attrs)
        type_attrs = self._get_attrs_by_type()
        attrs.update(self.attrs)
        cls = type_attrs.get('class', '')
        if self.widget_class:
            cls = cls + ' ' + self.widget_class
        if generic_widget_class:
            cls = cls + ' ' + generic_widget_class
        cls = cls.strip()
        if cls:
            attrs['class'] = ' '.join([cls, attrs.get('class', '')]).strip()
        if self.widget_attrs:
            attrs.update(self.widget_attrs)
        if extra_attrs:
            attrs.update(extra_attrs)
        return attrs

    def _get_attrs_by_type(self):
        if widget_type_attrs:
            return widget_type_attrs.get(self.type, {})
        return {}

    def render(self, value=None, attrs=None, control_only=False):
        if not self.field:
            return ''
        from prefs import pref
        if value is None:
            value = pref.get(self.name, self.default_value)
        field = self.get_field()
        widget_attrs = self.get_attrs(attrs)
        widget = field.widget.render(self.name, value, attrs=widget_attrs)
        namespace = (self.template_namespace or 'prefs/widgets/').rstrip('/') + '/'
        name = self.template_name or self.type
        tmp = loader.get_template(namespace+name+'.html')
        field.id = widget_attrs.get('id', '')
        return mark_safe(tmp.render(dict(
            field=field,
            pref_field=self,
            widget=widget,
            control_only=control_only
        )))

    def render_control(self, *args, **kwargs):
        return self.render(control_only=True, *args, **kwargs)

    def get_field(self):
        return self.field(widget=self.widget)

    def apply_state(self, conf_data):
        if self.disabled_when:
            try:
                self.disabled = bool(eval(self.disabled_when, conf_data, conf_data))
            except Exception as e:
                self.error = 'Error expression "{}": {}'.format(self.disabled_when, e)
        if self.hidden_when:
            try:
                self.hidden = bool(eval(self.hidden_when, conf_data, conf_data))
            except Exception as e:
                self.error = 'Error expression "{}": {}'.format(self.hidden_when, e)


class PrefStringField(PrefField):
    type = 'str'
    field = forms.CharField
    widget = forms.TextInput


class PrefIntField(PrefField):
    type = 'int'
    field = forms.IntegerField
    widget = forms.NumberInput


class PrefTextField(PrefField):
    type = 'txt'
    field = forms.CharField
    widget = forms.Textarea
    default_attrs = {'width': '98%'}


class PrefBoolField(PrefField):
    type = 'bol'
    field = forms.BooleanField
    widget = forms.CheckboxInput


class PrefSelectField(PrefField):
    type = 'sel'
    field = forms.ChoiceField
    widget = forms.Select

    def get_field(self):
        return self.field(widget=self.widget, choices=self.kwargs.get('choices', []))


class PrefSummernoteField(PrefField):
    type = 'smn'
    field = SummernoteTextFormField


# class PrefFileField(PrefField):
#     type = 'fil'
#     field = forms.FileField
#     widget = forms.FileInput







