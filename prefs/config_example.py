from collections import OrderedDict as od

config = od([
    ('Site Config', od([
        ('DOMAIN',
            dict(title='Домен', default='domain.com', type='str', copy_to_settings=False)),
        ('SITE_NAME',
            dict(title='Имя сайта', default='Site Name', type='str')),
        ('ADD_TUTORIAL_BUTTON',
            dict(title='Предложить урок', default=False, type='bol', help='Добавить кнопку "Предложить урок"')),
        ('FAVORITES',
            dict(title='Избранное', default=False, type='bol', help='Включить функционал избранного контента')),
        ])
     ),
    ('Debug', od([
        ('DEBUG',
            dict(title='Debug Mode', default=False, type='bol')),
        ])
     ),
])

# todo:
# summernote
# mirrotcode
# choices
