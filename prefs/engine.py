import redis, json, os
import logging
from pathlib import Path
import importlib.machinery
from django.conf import settings
from django.template.defaultfilters import slugify
from datetime import timedelta


logger = logging.getLogger(__name__)
r = redis.Redis(host=os.getenv('REDIS_HOST') or settings.REDIS_HOST or 'localhost',
                port=int(os.getenv('REDIS_PORT', 0)) or settings.REDIS_PORT or 6379,
                db=3)


class Preferences(object):
    """
    Динамические настройки сайта на базе Redis
    """
    __db_prefix = 'preferences:'
    _file_name = 'preferences'
    _prefs_file_name = 'pref_config.py'

    def __init__(self):
        self._is_init = False
        self._defaults = None

    def _init(self):
        self._is_init = True
        self._defaults = self._get_defaults()
        self._update()

    def __getattribute__(self, item: str):
        if item.isupper():
            return self.get(item)
        return super(Preferences, self).__getattribute__(item)

    def __getitem__(self, item):
        if not self._is_init:
            self._init()
        if item.isupper():
            return self.get(item)
        raise KeyError

    @property
    def r(self):
        return r

    def _get_defaults(self):
        """Получить значения по умолчанию"""
        conf = self._read_conf()
        defaults = {}
        for tab in conf:
            for section in tab.get('content', []):
                for field in section['fields']:
                    defaults[field.name] = field.default_value
        return defaults

    def set(self, key, value, expire=None):
        """Сохранение нового значения параметра"""
        if not self._is_init:
            self._init()
        res = r.set(self.__db_prefix + key, json.dumps(value))
        if expire:
            if isinstance(expire, timedelta):
                sec = expire.total_seconds()
            elif isinstance(expire, (int, float)):
                sec = int(expire)
            else:
                raise TypeError('expire must be number or timedelta')
            r.expire(self.__db_prefix + key, int(sec))
        return res

    def get(self, key, default=None, raise_if_no_key=False):
        """Чтение параметра по ключу"""
        # todo: add cache with timeout
        if not self._is_init:
            self._init()
        val = r.get(self.__db_prefix + key)
        if val is None:
            if default is not None:
                return default
            if key not in self._defaults:
                if raise_if_no_key:
                    raise KeyError(f'No value "{key}"')
                else:
                    return default
            return self._defaults.get(key)
        return json.loads(val.decode())

    def clear_key(self, key):
        r.delete(self.__db_prefix + key)

    def _save(self, data):
        """Сохранение настроек в JSON"""
        prefs = self._post_to_prefs(data)
        pref_file = self._pref_file()
        os.makedirs(os.path.dirname(pref_file), mode=0o777, exist_ok=True)
        json.dump(prefs, open(pref_file, 'w'), indent=2)
        self._update()

    def _update(self):
        # todo: reset cache
        data = self._read_prefs() or {}
        for k, v in data.items():
            self.set(k, v)

    @classmethod
    def _read_prefs(cls):
        """Чтение текущих настроек"""
        pref_file = cls._pref_file()
        if pref_file and os.path.exists(pref_file):
            try:
                data = json.load(open(pref_file))
                return data
            except Exception as e:
                logger.error(str(e))
        return {}

    @classmethod
    def _read_conf(cls):
        from django.apps import apps
        conf_files = []
        for app in apps.get_app_configs():
            conf = Path(app.path) / cls._prefs_file_name
            if conf.exists():
                conf_files.append(conf)
        generic_conf = []
        site_prefs = Path(settings.BASE_DIR) / cls._prefs_file_name
        if site_prefs.exists():
            mod = importlib.machinery.SourceFileLoader(site_prefs.parent.stem + '_' + site_prefs.stem, site_prefs.as_posix()).load_module()
            conf = getattr(mod, 'config', [])
            generic_conf.extend(conf)
        for c in conf_files:
            mod = importlib.machinery.SourceFileLoader(c.parent.stem+'_'+c.stem, c.as_posix()).load_module()
            conf = getattr(mod, 'config', [])
            generic_conf.extend(conf)
        logger.info(f'Loaded {len(conf_files)} files')
        generic_conf.sort(key=lambda x: x.get('order_priority', 50), reverse=True)
        return generic_conf

    @classmethod
    def _pref_file(cls):
        pref_file = os.getenv('PREFERENCES_DATA_FILE')\
                    or getattr(settings, 'PREFERENCES_DATA_FILE', None)\
                    or os.path.join(settings.BASE_DIR, cls._file_name+'.json')
        return pref_file

    @classmethod
    def _post_to_prefs(cls, post):
        """
        Сохранение изменений
        """
        prefs = {}
        for name, value in sorted(post.items(), key=lambda x: x[0]):
            if name == 'csrfmiddlewaretoken':
                continue
            if not name.isupper():
                continue
            if isinstance(value, str):
                if value.isdigit():
                    value = int(value)
                elif value.rstrip('.').count('.') == 1:
                    if ''.join(value.split('.', 1)).isdigit():
                        try:
                            value = float(value)
                        except:
                            pass
                if value == 'on':
                    value = True
                elif value == 'off':
                    value = False
            prefs[name] = value
        for tab in cls._read_conf():
            content = tab.get('content')
            if not content:
                continue
            for section in content:
                for field in section['fields']:
                    if field.type == 'bol':
                        if field.name not in post:
                            prefs[field.name] = False
        return prefs

    def get_fields(self, template_namespace=None):
        tabs, data = self._read_conf(), self._read_prefs()
        tab_ids = []
        for i, tab in enumerate(tabs):
            for section in tab['content']:
                for field in section['fields']:  # type: fields.PrefField
                    field.set_value(data.get(field.name, field.default_value))
                    field.set_template_namespace(template_namespace)
                    field.apply_state(data)
            tab['index'] = i
            t_id = slugify(tab['title'])
            inc = 1
            name = t_id
            while name in tab_ids:
                name = f'{t_id}-{inc}'
                inc += 1
            tab['id'] = name
        return tabs

