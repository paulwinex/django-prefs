from django.views.generic import TemplateView, View
from django.http import JsonResponse, Http404
from prefs import pref
from django.contrib import admin
import traceback
from django.conf import settings
from django.shortcuts import redirect
import logging
logger = logging.getLogger(__name__)


class PrefsView(TemplateView):
    template_name = 'prefs/prefs.html'

    def post(self, request):
        post = request.POST.dict()
        try:
            pref._save(post)
        except Exception as e:
            traceback.print_exc()
            return JsonResponse({'result': 0, 'error': 'Cant save prefs: {}'.format(e)})

        if request.is_ajax():
            return JsonResponse({'result': 1})
        else:
            return redirect(request.path)

    def get_context_data(self, **kwargs):
        if not self.request.user.is_staff:
            raise Http404()
        ctx = super(PrefsView, self).get_context_data()
        ctx.update(admin.site.each_context(self.request))
        ctx['tabs'] = pref.get_fields()
        ctx['title'] = 'Dynamic Preferences'
        return ctx

    def get_context_data1(self, **kwargs):
        ctx = super(PrefsView, self).get_context_data()
        ctx.update(admin.site.each_context(self.request))
        ctx['locked'] = True
        ctx['title'] = 'Dynamic Preferences'

        if self.request.user.is_authenticated and self.request.user.is_staff:
            ctx['is_active'] = True
            ctx['settings'] = settings
            ctx['locked'] = False
            config = list(pref._read_conf().items())
            prefs = pref._read_prefs() or {}
            prefs = list(pref._read_conf().items())
            _tabs = []
            tab_content = []
            for elem in prefs:
                if not elem[1]:
                    if tab_content:
                        _tabs.append(tab_content)
                        tab_content = []
                tab_content.append(elem)
            _tabs.append(tab_content)
            tabs = []
            for i, tab in enumerate(_tabs):
                tab_content = dict(
                    title='',
                    index=i,
                    content=[]
                )

                for item in tab:
                    if not item[1]:
                        tab_content['title'] = item[0]
                    else:
                        tab_content['content'].append(item)
                tabs.append(tab_content)
            ctx['tabs'] = tabs
            from pprint import pprint as pp
            pp(tabs)
            print('PREFS')
            pp(ctx['prefs'])
            ctx['site_url'] = f"{self.request.scheme}://{self.request.META['HTTP_HOST']}"
            return ctx
        else:
            raise Http404()


class AccessCheckMixin:
    pref_access_key = None
    pref_access_admin_key = None

    def dispatch(self, request, *args, **kwargs):
        if self.pref_access_key:
            if not pref.get(self.pref_access_key):
                if pref.get(self.pref_access_admin_key) and request.user.is_staff:
                    return super(AccessCheckMixin, self).dispatch(request, *args, **kwargs)
                raise Http404
        return super(AccessCheckMixin, self).dispatch(request, *args, **kwargs)
