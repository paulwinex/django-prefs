from django import template
from django.utils.safestring import mark_safe


register = template.Library()


@register.simple_tag(takes_context=True)
def pref_field(context, field):
    return mark_safe(field.render())
