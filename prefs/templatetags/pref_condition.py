from django import template
from django.template.base import kwarg_re
from django.utils.safestring import mark_safe
from prefs import pref


register = template.Library()


@register.tag('enableif')
def do_enable_if(parser, token):
    tag_name, args, kwargs = parse_args(token, parser)
    if not args:
        raise template.TemplateSyntaxError("Not enough arguments")
    nodelist = parser.parse(('endenableif',))
    parser.delete_first_token()
    return SwitchNode(nodelist, *args, **kwargs)


def parse_args(token, parser):
    parts = token.split_contents()
    tag_name = parts.pop(0)
    args = []
    kwargs = {}
    for part in parts:
        # Is this a kwarg or an arg?
        match = kwarg_re.match(part)
        kwarg_format = match and match.group(1)
        if kwarg_format:
            key, value = match.groups()
            # kwargs[key] = FilterExpression(value, parser)
            kwargs[key] = value
        else:
            # args.append(FilterExpression(part, parser))
            args.append(part)
    return (tag_name, args, kwargs)


class SwitchNode(template.Node):
    def __init__(self, nodelist, *args, **kwargs):
        self.nodelist = nodelist
        self.key = args[0]
        self.staff_key = kwargs.get('staff_user')

    def render(self, context):
        # Evaluate the arguments in the current context

        if not pref.get(self.key):
            if self.staff_key and (pref.get(self.staff_key) and context['user'].is_staff):
                return mark_safe(self.nodelist.render(context))
            else:
                return ''
        return mark_safe(self.nodelist.render(context))

