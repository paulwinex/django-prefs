from suit.menu import ParentItem, ChildItem
from django.shortcuts import reverse


def get_suit_menu():
    par = ParentItem('Preferences', icon='fa fa-chevron-right', url=reverse('prefs'))
    return [par]
