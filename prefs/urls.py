from django.urls import path
from .views import PrefsView
# from .views import PrefsApi

app_name = "prefs"

urlpatterns = [
    path('', PrefsView.as_view(), name='index'),
]