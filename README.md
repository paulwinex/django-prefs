# Django Prefs

Dynamic preferences based on Redis

#### Install

- install app
- add app to INSTALLED_APPS
- add /prefs/static to STATICFILES_DIRS
- add menu item to admin
- create prefs_config.py in other apps
