from distutils.core import setup

setup(name='DjangoDynPref',
      version='1.0',
      description='Django Dynamic Preferences',
      author='paulwinex',
      author_email='paulwinex@gmail.com',
      url='https://gitlab.com/paulwinex/django-prefs.git',
      packages=['prefs', 'prefs.templatetags'],
      include_package_data=True
     )
